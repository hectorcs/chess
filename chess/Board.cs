﻿using System;

namespace chess
{
    public class Board
    {
        static public string LETTERS = "abcdefgh";
        static public int Size;

        public enum BoardSideType
        {
            Top = 7,
            Bottom = 0
        }

        private Cell[,] _cells;

        public Board()
        {
            Size = Board.LETTERS.Length;
            _cells = new Cell[Size, Size];

            // creates the cells
            for (var row = 0; row < Size; row++)
            {
                for (var column = 0; column < Size; column++)
                {
                    _cells[row, column] = new Cell(row, column);
                }
            }

            // create the playing pieces and places them in the corresponding cells
            CreatePieces(BoardSideType.Bottom, Piece.ColorType.White);
            CreatePieces(BoardSideType.Top, Piece.ColorType.Black);
        }

        public void CreatePieces(BoardSideType boardSide, Piece.ColorType color)
        {
            var rowStart = (int)boardSide;
            var direction = boardSide == BoardSideType.Bottom? 1 : -1;

            for (var rowRelative = 0; rowRelative < 2; rowRelative++)
            {
                for (var column = 0; column < Size; column++)
                {
                    var row = rowStart + rowRelative * direction;
                    var cell = _cells[row, column];

                    Type pieceType = null;

                    switch(rowRelative)
                    {
                        case 0: // first row. add the important pieces

                            switch (column)
                            {
                                case 0:
                                case 7:
                                    pieceType = typeof(Piece.Rook);
                                    break;

                                case 1:
                                case 6:
                                    pieceType = typeof(Piece.Knight);
                                    break;

                                case 2:
                                case 5:
                                    pieceType = typeof(Piece.Bishop);
                                    break;

                                case 3:
                                    pieceType = typeof(Piece.Queen);
                                    break;

                                case 4:
                                    pieceType = typeof(Piece.King);
                                    break;
                            }

                            break;
    
                        case 1: // second row. add all the pawns
                            pieceType = typeof(Piece.Pawn);
                            break;
                    }

                    if (pieceType != null)
                    {
                        var piece = Activator.CreateInstance(pieceType) as Piece;
                        piece.Color = color;
                        piece.Direction = direction;
                        piece.Board = this;
                        piece.PlaceInCell(cell);
                    }
                }
            }
        }

        internal Cell GetCellAt(int row, int column)
        {
            try
            {
                return _cells[row, column];
            }
            catch (Exception)
            {
                return null;
            }
        }

        internal Cell GetCellByPosition(string position)
        {
            if (!string.IsNullOrEmpty(position) && position.Length == 2)
            {
                var columnLetter = position.Substring(0, 1);
                var rowNumberAsString = position.Substring(1, 1);

                var column = -1;
                var row = -1;

                column = LETTERS.IndexOf(columnLetter, StringComparison.CurrentCultureIgnoreCase);

                if (Int32.TryParse(rowNumberAsString, out row) )
                {
                    row--; // because input is 1 based but internal row is zero based.
                }

                return GetCellAt(row, column);
            }
            return null;
        }

        public class Cell
        {
            private Piece _piece;
            public Piece Piece
            {
                get { return _piece; }
                set { _piece = value; }
            }

            public Cell(int row, int column)
            {
                Row = row;
                Column = column;
            }

            public int Row { get; internal set; }
            public int Column { get; internal set; }

            public override string ToString()
            {
                return Board.LETTERS.Substring(Column, 1) + ((Row + 1).ToString());
            }
        }

        internal ValidityCheckResult CheckMoveValidity(Piece pieceToMove, Cell end)
        {
            var result = new ValidityCheckResult();

            if (pieceToMove.CurrentCell == end)
            {
                result.IsValid = false;
                result.Feedback = "Target position is the same as the starting one.";
            }
            else
            {
                result.IsValid = true;
            }

            return pieceToMove.ValidateAbilityToMove(end);
        }

        public struct ValidityCheckResult
        {
            public bool IsValid;
            public string Feedback;
        }
    }
}