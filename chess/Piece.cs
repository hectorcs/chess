﻿using System;
using static chess.Board;

namespace chess
{
    public class Piece
    {
        public enum ColorType
        {
            Black,
            White
        }

        public Piece()
        {
        }

        private Cell _currentCell;
        public Cell CurrentCell
        {
            get { return _currentCell; }
            internal set
            {
                if (_currentCell != null)
                {
                    _currentCell.Piece = null;
                    MoveCount++;
                }
                _currentCell = value;
                _currentCell.Piece = this;
            }
        }

        public ColorType Color { get; internal set; }
        public int Direction { get; internal set; }

        public int MoveCount { get; internal set; }

        public string Name { get; internal set; }

        public Board Board;

        public bool CanLeap { get; internal set; }

        public void PlaceInCell(Cell cell)
        {
            CurrentCell = cell;
        }

        protected MovementData _movementData;
        public struct MovementData
        {
            public bool IsChangingRow;
            public bool IsChangingColumn;
            public int AxesCount;
            public int ColumnsTravelled;
            public int RowsTravelled;
            public Cell Destination;
            public int RowOffset;
        }

        protected MovementData AnalyseMovementData(Cell destinationCell)
        {
            var data = new MovementData();

            // destination cell
            data.Destination = destinationCell;

            // distance travelled
            data.RowsTravelled = Math.Abs(this.CurrentCell.Row - destinationCell.Row);
            data.ColumnsTravelled = Math.Abs(this.CurrentCell.Column - destinationCell.Column);

            // horizontal and vertical movement
            data.IsChangingRow = CurrentCell.Row != destinationCell.Row;
            data.IsChangingColumn = CurrentCell.Column != destinationCell.Column;

            // how many axis would change
            var axesCount = 0;
            axesCount += data.IsChangingRow ? 1 : 0;
            axesCount += data.IsChangingColumn ? 1 : 0;
            data.AxesCount = axesCount;

            data.RowOffset = destinationCell.Row - this.CurrentCell.Row;

            return data;
        }

        public ValidityCheckResult ValidateAbilityToMove(Cell destinationCell)
        {
            var result = new ValidityCheckResult();

            var isWithinPieceAbility = this.CheckCanMoveTo(destinationCell);
            if (isWithinPieceAbility)
            {
                var isPathClear = this.CheckForObstructionsTo(destinationCell) == false;
                if (isPathClear)
                {
                    result.IsValid = true;
                }
                else
                {
                    result.Feedback = "The piece cannot move through another piece.";
                }
            }
            else
            {
                result.Feedback = "This piece does not have the ability to move to that position."; // TODO explain piece abilities
            }
            return result;
        }

        public bool CheckCanMoveTo(Cell cell)
        {
            _movementData = AnalyseMovementData(cell);
            var canPerformMovement = ValidateMovementData();
            return canPerformMovement;
        }

        virtual public bool CheckIfMovementCanCapture()
        {
            return true; // by default all moves are capture moves. however, the pawn can only capture when moving diagonally.
        }

        virtual public bool ValidateMovementData()
        {
            throw new NotImplementedException();
        }

        virtual public bool CheckForObstructionsTo(Cell cell)
        {
            var rowChecking = this.CurrentCell.Row;
            var columnChecking = this.CurrentCell.Column;

            if (this.CanLeap)
            {
                rowChecking = cell.Row;
                columnChecking = cell.Column;
            }

            var checking = true;
            while (checking)
            {
                if (rowChecking != cell.Row)
                {
                    rowChecking += (Math.Sign(cell.Row - rowChecking) * 1);
                }

                if (columnChecking != cell.Column)
                {
                    columnChecking += (Math.Sign(cell.Column - columnChecking) * 1);
                }


                var pieceAtCellBeingChecked = Board.GetCellAt(rowChecking, columnChecking).Piece;
                var isThereAPiece = pieceAtCellBeingChecked != null;

                var reachedCell = (rowChecking == cell.Row && columnChecking == cell.Column);

                if (isThereAPiece)
                {
                    if (reachedCell)
                    {
                        var isSameColor = (pieceAtCellBeingChecked.Color == this.Color);
                        if (isSameColor)
                        {
                            // a same color piece cannot be captured so it's considered an obstruction
                            return true;
                        }
                        else
                        {
                            // it is an opponent, however, the momement has to be a capture move in order to take it.
                            if (!CheckIfMovementCanCapture())
                            {
                                // so, if it's not a capture move, it's considered an obstruction
                                return true;
                            }
                        }
                    }
                    else
                    {
                        // any color piece on the way is an obstruction
                        return true;
                    }
                }

                if (reachedCell)
                {
                    checking = false;
                }
            }

            return false; // if reached this point, there's no obstructions.
        }

        internal class Rook : Piece
        {
            public Rook() : base ()
            {
                Name = "Rook";
            }

            public override bool ValidateMovementData()
            {
                return _movementData.AxesCount == 1;
            }
        }

        internal class Knight : Piece
        {
            public Knight() : base ()
            {
                Name = "knight";
                CanLeap = true;
            }

            public override bool ValidateMovementData()
            {
                return
                    (_movementData.ColumnsTravelled == 2 && _movementData.RowsTravelled == 1) ||
                    (_movementData.ColumnsTravelled == 1 && _movementData.RowsTravelled == 2);
            }
        }

        internal class Bishop : Piece
        {
            public Bishop() : base()
            {
                Name = "Bishop";
            }

            public override bool ValidateMovementData()
            {
                return _movementData.ColumnsTravelled == _movementData.RowsTravelled;
            }
        }

        internal class Queen : Piece
        {
            public Queen() : base()
            {
                Name = "Queen";
            }

            public override bool ValidateMovementData()
            {
                switch(_movementData.AxesCount)
                {
                    case 1: // horizontal or vertical
                        return true;

                    case 2: // diagonal movement
                        return _movementData.ColumnsTravelled == _movementData.RowsTravelled;
                }
                return false;
            }
        }

        internal class King : Piece
        {
            public King() : base()
            {
                Name = "King";
            }

            public override bool ValidateMovementData()
            {
                switch (_movementData.AxesCount)
                {
                    case 1: // horizontal or vertical
                        return (_movementData.ColumnsTravelled + _movementData.RowsTravelled) == 1;

                    case 2: // diagonal movement
                        return (_movementData.ColumnsTravelled + _movementData.RowsTravelled) == 2;
                }
                return false;
            }
        }

        internal class Pawn : Piece
        {
            public Pawn() : base()
            {
                Name = "Pawn";
            }

            public override bool ValidateMovementData()
            {
                var isMovingForward = Math.Sign(Direction) == Math.Sign(_movementData.RowOffset);
                if (isMovingForward)
                {
                    var isWithinAllowedHorizontalRange = _movementData.ColumnsTravelled <= 1;
                    var maximumStepsForwardAllowed = (MoveCount == 0 && _movementData.ColumnsTravelled == 0)? 2 : 1;
                    var isWithinAllowedVerticalRange = _movementData.RowsTravelled <= maximumStepsForwardAllowed;

                    var isWithinAllowedRange = isWithinAllowedHorizontalRange && isWithinAllowedVerticalRange;
                    if (isWithinAllowedRange)
                    {
                        if (CheckIfMovementCanCapture() && _movementData.Destination.Piece == null)
                        {
                            // it's a capture movement but no piece at destination: illegal move
                            return false;
                        }
                        return true;
                    }
                }
                return false;
            }

            public override bool CheckIfMovementCanCapture()
            {
                if (_movementData.ColumnsTravelled == 1 && _movementData.RowsTravelled == 1 )
                {
                    return true;
                }
                return false;
            }
        }

        internal string GetName()
        {
            var colorName = this.Color.ToString();
            var pieceName = this.Name;
            return string.Format("{0} {1}", colorName, pieceName);
        }

        internal string GetShortName()
        {
            var colorName = this.Color.ToString();
            var pieceName = this.Name;
            return string.Format("{0}{1}", colorName[0], pieceName[0]);
        }
    }
}
