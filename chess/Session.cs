﻿using System;
using System.Collections.Generic;
using System.Text;

namespace chess
{
    public class Session
    {
        private int _playerIndex;
        private List<Player> _players = new List<Player>();
        private Board _board;

        public Player CurrentPlayer => _players[_playerIndex];

        public Session()
        {
            _board = new Board();

            _players.Add(new Player(Piece.ColorType.White));
            _players.Add(new Player(Piece.ColorType.Black));

            IsOngoing = true;
        }

        public bool IsOngoing { get; internal set; }

        public void RenderBoard()
        {
            Console.Clear();

            var shouldDrawnTop = true;
            for (var row = Board.Size-1; row >= 0; row--)
            {
                if (shouldDrawnTop)
                {
                    for (var line = 0; line < 2; line++)
                    {
                        WriteLine(line, row, false);
                        for (var column = 0; column < Board.Size; column++)
                        {
                            switch (line)
                            {
                                case 0:
                                    Console.Write(" {0}   ", Board.LETTERS.Substring(column, 1).ToUpper());
                                    break;
                                case 1:
                                    Console.Write("_____");
                                    break;
                            }
                        }
                    }
                    shouldDrawnTop = false;
                }

                for (var line = 0; line < 2 ; line++)
                {
                    WriteLine(line, row);
                    for (var column = 0; column < Board.Size; column++)
                    {
                        switch(line)
                        {
                            case 0:
                                var cell = _board.GetCellAt(row, column);
                                //Console.Write("<{0}-{1}>", column, row);
                                var piece = cell.Piece;
                                var pieceName = piece == null ? "  " : piece.GetShortName();
                                Console.Write(" {0} |", pieceName);
                                break;

                            case 1:
                                Console.Write("____|");
                                break;
                        }
                    }
                }
            }

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Current player color: {0}.", CurrentPlayer.Color);
        }

        private void WriteLine(int line, int row, bool shouldWriteRow = true)
        {
            Console.WriteLine();
            var fill = (line == 0 && shouldWriteRow) ? (row+1).ToString() : " ";
            var divider = shouldWriteRow ? "|" : " ";
            Console.Write("{0}  {1}", fill, divider);
        }

        private StringBuilder _feedback = new StringBuilder();
        internal void RequestInput()
        {

            Console.Write("please enter your move: ");
            var input = new UserInput();
            input.Given = Console.ReadLine();

            switch(input.Command)
            {
                case UserInput.CommandType.Move:
                    var positions = input.Given.Split(' ');

                    var move = new Move();
                    if (positions.Length == 2)
                    {
                        move.Start = _board.GetCellByPosition(positions[0]);
                        move.End = _board.GetCellByPosition(positions[1]);
                    }

                    AttemptMove(move);
                    break;

                case UserInput.CommandType.EndSession:
                    IsOngoing = false;
                    break;

                case UserInput.CommandType.Invalid:
                default:
                    // invalid command
                    _feedback.AppendLine("Invalid command. Input example: \"E4 \"E5\"");
                    break;
            }

            if (_feedback.Length > 0)
            {
                Console.WriteLine(_feedback.ToString());
                Console.WriteLine("Please press any key to continue.");
                Console.ReadKey();

                _feedback.Clear();
            }
        }

        private void AttemptMove(Move move)
        {
            if (move.Start == null)
            {
                _feedback.AppendLine("Start position is invalid.");
            }
            if (move.End == null)
            {
                _feedback.AppendLine("End position is invalid.");
            }

            if (move.Start != null && move.End != null)
            {
                var pieceToMove = move.Start.Piece;
                if (pieceToMove == null)
                {
                    _feedback.AppendLine(string.Format("There is no piece in cell {0}.", move.Start.ToString()));
                    return;
                }

                if (pieceToMove.Color != CurrentPlayer.Color)
                {
                    _feedback.AppendLine(string.Format("It's {0} color's turn. You can't move a {1} piece.", CurrentPlayer.Color, pieceToMove.GetName()));
                    return;
                }

                var result = _board.CheckMoveValidity(pieceToMove, move.End);
                if (!result.IsValid)
                {
                    _feedback.AppendLine(string.Format("Invalid move for {0}.", pieceToMove.GetName()));
                    if (!string.IsNullOrEmpty(result.Feedback))
                    {
                        _feedback.AppendLine(result.Feedback);
                    }
                    return;
                }

                pieceToMove.PlaceInCell(move.End);
                HandleMoveComplete();
            }
        }

        private void HandleMoveComplete()
        {
            // next player
            _playerIndex++;
            _playerIndex %= _players.Count;
        }
    }

    struct UserInput
    {
        public enum CommandType
        {
            Invalid,
            Move,
            EndSession
        }

        private string _given;
        public string Given
        {
           get { return _given; }
           set
           {
                _given = value;

                if (string.IsNullOrEmpty(_given)) return;

                _given = _given.Trim().ToLower();

                if(_given.Equals("quit"))
                {
                    Command = CommandType.EndSession;
                    return;
                }

                if (_given.Length == 5)
                {
                    Command = CommandType.Move;
                    return;
                }
           }
        }
        public CommandType Command;
    }

    struct Move
    {
        public Board.Cell Start;
        public Board.Cell End;
    }
}
