﻿namespace chess
{
    public class Player
    {
        public Piece.ColorType Color;

        public Player(Piece.ColorType color)
        {
            this.Color = color;
        }
    }
}