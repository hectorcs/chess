﻿using System;

namespace chess
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            var session = new Session();

            while(session.IsOngoing)
            {
                session.RenderBoard();
                session.RequestInput();
            }

            Console.WriteLine("end of game");
        }
    }
}
